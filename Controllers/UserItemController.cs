﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UsersService.Data;
using UsersService.Models;
using UsersService.Services;

namespace UsersService.Controllers
{
    [Route("[controller]")]
    [Controller]
    public class UserController : ControllerBase
    {
        private readonly UserItemData _userItemData;

        public UserController(MyDBContext context)
        {
            _userItemData = new UserItemData(context);
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <remarks>By passing in the appropriate options, you can list the conversation</remarks>
        /// <param name="skip"> number of records to skip for pagination</param>
        /// <param name="limit">maximum number of records to return</param>
        [HttpGet]
        [ProducesResponseType(typeof(List<UserItem>), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<List<UserItem>> GetUsers(int skip, int limit)
        {
            try
            {
                var res = _userItemData.findAll(skip, limit);
                if (res == null)
                    return BadRequest("bad input parameter");
                return Ok(res);
            }
            catch (Exception)
            {
                return BadRequest("bad input parameter");
            }
        }

        /// <summary>
        /// Create a new user
        /// </summary>
        /// <remarks>Create new user</remarks>
        /// <param name="userItem">user minimal info</param>
        [HttpPost]
        [ProducesResponseType(typeof(CreatedResult), 201)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<UserItem> PostUserItem(UserItem userItem)
        {
            if (_userItemData.UserItemExists(userItem.id))
                return BadRequest("an existing item already exists");
            var res = _userItemData.addUserItem(userItem);
            if (res == null)
                return BadRequest("invalid input, object invalid");
            return Created(res.id, res);
        }


        /// <summary>
        /// Get a user info
        /// </summary>
        /// <remarks>By passing in the appropriate options, you can list one particular user</remarks>
        /// <param name="id">user id</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserItem), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<UserItem> GetUserItem(string id)
        {
            if (!_userItemData.UserItemExists(id))
                return NotFound("item does not exists");

            return Ok(_userItemData.findById(id));
        }

        /// <summary>
        /// Update user info
        /// </summary>
        /// <remarks>Add some change to the user</remarks>
        /// <param name="id">user id to update</param>
        /// <param name="userItem">Update the user item</param>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UserItem), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        public IActionResult PutUserItem(string id,[FromBody]UserItem userItem)
        {
            if (id != userItem.id)
                return BadRequest("invalid input, object invalid");
            else if (!_userItemData.UserItemExists(id))
                return NotFound("item does not exists");
            else if (_userItemData.updateUserItem(userItem))
                return Ok(userItem);
            return BadRequest("invalid input, object invalid");

        }

      

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <remarks>Delete one particular user</remarks>
        /// <param name="id">user id to delete</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(UserItem), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        public ActionResult<UserItem> DeleteUserItem(string id)
        {
            if (!_userItemData.UserItemExists(id))
                return NotFound("item does not exists");
            else if(!_userItemData.deleteUserItem(id))
                return BadRequest("invalid input, object invalid");
            return Ok();
        }

    }
}
