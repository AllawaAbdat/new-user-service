# Stage 1
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /build
COPY . .
RUN rm -rf ./Test
RUN dotnet --version
RUN dotnet restore "UsersService.csproj"
RUN dotnet publish "UsersService.csproj" -c Release -o /app

FROM build AS testrunner
WORKDIR /build/tests
ENTRYPOINT ["dotnet", "test", "--logger:trx"]

FROM build AS test
WORKDIR /build/tests
COPY /Test/*.csproj .
RUN dotnet test "UsersService.Test.csproj"

# Stage 2
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS final
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "UsersService.dll"]
