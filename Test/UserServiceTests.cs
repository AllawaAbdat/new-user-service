using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using UsersService.Data;
using UsersService.Models;
using UsersService.Services;
using Xunit;

namespace UsersService.Test
{
    public class UserServiceTests
    {
        private readonly UserItemData _sut;
        private readonly MyDBContext _context;
        protected IServiceProvider ServiceProvider { get; }

        protected SqliteConnection Connection { get; }

        protected string ConnectionStringSqlite { get; }


        public UserServiceTests()
        {
            ConnectionStringSqlite = $"Server=tcp:rattrapage.database.windows.net,1433;Initial Catalog=RTP_CLO5_USER_SERVICE;Persist Security Info=False;User ID=allawa;Password=14752083Ff.;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            var services = new ServiceCollection();
            services.AddDbContext<MyDBContext>(opt =>
               opt.UseSqlServer(ConnectionStringSqlite)
           ) ;

            IServiceProvider ServiceProvider = services.BuildServiceProvider();

            var connection = new Microsoft.Data.SqlClient.SqlConnection(ConnectionStringSqlite);
            connection.Open();

            var options = new DbContextOptionsBuilder<MyDBContext>()
                    .UseSqlServer(connection)
                    .Options;
            _context = new MyDBContext(options);
            _sut = new UserItemData(_context);
        }

        [Fact]
        public void AddUserShouldAddUserToDb()
        {
            UserItem userToAdd = new UserItem
            {
                firstname = "mame",
                lastname = "sene",
                email = "etna@etna.com"
            };

            var resultUser = _sut.addUserItem(userToAdd);
            Assert.Equal(userToAdd.firstname, resultUser.firstname);
            Assert.Equal(userToAdd.lastname, resultUser.lastname);
            Assert.Equal(userToAdd.email, resultUser.email);
            Assert.NotNull(resultUser.id);
         
        }
    }
}
