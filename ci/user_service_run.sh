#!/bin/sh

set -e

  docker build -t my-user-service .
  docker run -d --name my-running-user-service my-user-service
  if docker ps | grep -q my-running-user-service; then
        echo Docker my-running-user-service found
        # docker exec my-running-user-service dotnet test
    else
        echo Docker my-running-user-service not found
        exit
  fi