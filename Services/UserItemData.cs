﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UsersService.Data;
using UsersService.Models;

namespace UsersService.Services
{
    public class UserItemData
    {
        private readonly MyDBContext _context;
        public UserItemData(MyDBContext context)
        {
            _context = context;

        }
        public List<UserItem> findAll(int skip, int limit)
        {
            var res = _context.UserItems
                        .Skip(skip)
                        .Take(limit)
                        .ToList();

            if (res.Count < 1)
                return null;
            return res;
        }

        public UserItem findById(string id)
        {
            return _context.UserItems.Find(id);
        }

        public UserItem addUserItem(UserItem userToAdd)
        {
            try
            {
                UserItem newUser = new UserItem
                {
                    email = userToAdd.email,
                    firstname = userToAdd.firstname,
                    lastname = userToAdd.lastname,
                    created_date = DateTime.Now
            };
                _context.UserItems.Add(newUser);
                _context.SaveChanges();
                return newUser;
            }
            catch (Exception)
            {
                return null;
            }
            
        }
        public bool updateUserItem(UserItem userToUpdate)
        {
            try
            {
                _context.UserItems.Update(userToUpdate);
                _context.SaveChanges();
                return true;

            }
            catch (Exception)
            {
               return false;
            }
        }

        public bool deleteUserItem(string id)
        {
            try
            {
                var userItemToDelete = this.findById(id);
                _context.UserItems.Remove(userItemToDelete);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UserItemExists(string id)
        {
            return _context.UserItems.Any(e => e.id == id);
        }

    }
}
